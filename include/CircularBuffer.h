// Circular Buffer.h
//
// John Haroian
// Nov 23, 2020

#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <stdint.h>

#define CIRCBUF_MAX_HANDLES 	10

#define CIRCBUF_OUT_OF_HANDLES 	-1
#define CIRCBUF_OK				0
#define CIRCBUF_BUFFER_FULL 	1
#define CIRCBUF_BUFFER_EMPTY 	2

typedef int16_t circbuf_t;

extern void CircularBuffer_Initialize(void);
extern circbuf_t CircularBuffer_Create(uint16_t* buffer);
extern uint16_t CircularBuffer_Items(circbuf_t circbuf_handle);
extern uint16_t CircularBuffer_Enqueue(circbuf_t circbuf_handle, uint16_t item);
extern uint16_t CircularBuffer_Dequeue(circbuf_t circbuf_handle, uint16_t* item);

#endif // CIRCULAR_BUFFER_H