// LEDDriver.h

#ifndef LEDDRIVER_H
#define LEDDRIVER_H

#include <stdint.h>

extern void LEDDriver_Create(uint16_t* ledAddr);
extern void LEDDriver_TurnOn(uint16_t ledNumber);
extern void LEDDriver_TurnOff(uint16_t ledNumber);

#endif // LEDDRIVER_H