#include "LEDDriver.h"

static uint16_t* ledAddress;

void LEDDriver_Create(uint16_t* ledAddr) {
	ledAddress = ledAddr;
	*ledAddress = 0x0000;
}

void LEDDriver_Destroy(void) {
	
}

void LEDDriver_TurnOn(uint16_t ledNumber) {
	*ledAddress = 1;
}

void LEDDriver_TurnOff(uint16_t ledNumber) {
	*ledAddress = 0;	
}
