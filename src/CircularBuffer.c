// Circular Buffer.c
//
// John Haroian
// Nov 23, 2020

#include <stddef.h>
#include "CircularBuffer.h"

struct sCircBuf {
	uint16_t* buffer;
	uint16_t* head;
	uint16_t* end;
};

static struct sCircBuf circbuf_handles[CIRCBUF_MAX_HANDLES];
static circbuf_t circbuf_handle_index;

extern void CircularBuffer_Initialize(void) {
	uint16_t i;

	for (i=0; i < CIRCBUF_MAX_HANDLES; i++) {
		circbuf_handles[i].buffer = NULL;
		circbuf_handles[i].head = NULL;
		circbuf_handles[i].end = NULL;		
	}
	circbuf_handle_index = -1;
}

circbuf_t CircularBuffer_Create(uint16_t* buffer) {
	circbuf_handle_index++;
	if(circbuf_handle_index < CIRCBUF_MAX_HANDLES) {
		circbuf_handles[circbuf_handle_index].buffer = buffer;
		circbuf_handles[circbuf_handle_index].head = buffer;
		circbuf_handles[circbuf_handle_index].end = buffer;
		return(circbuf_handle_index);
	} else {
		return(CIRCBUF_OUT_OF_HANDLES);
	}
}

uint16_t CircularBuffer_Items(circbuf_t circbuf_handle) {
	return(circbuf_handles[circbuf_handle].head - circbuf_handles[circbuf_handle].end);
}

uint16_t CircularBuffer_Enqueue(circbuf_t circbuf_handle, uint16_t item) {
	return(CIRCBUF_BUFFER_FULL);
}

uint16_t CircularBuffer_Dequeue(circbuf_t circbuf_handle, uint16_t* item) {
	return(CIRCBUF_BUFFER_EMPTY);
}
