// Circular Buffer of Ints
//
// Test list
// - Buffer is setup and empty after initialization
// - Add an int to the buffer
// - Dequeue an int from the buffer
// - Test buffer is full
// - Test buffer is empty

#include "unity_fixture.h"

#include "CircularBuffer.h"

TEST_GROUP(CircularBuffer);
	
TEST_SETUP(CircularBuffer) {
	CircularBuffer_Initialize();
}

TEST_TEAR_DOWN(CircularBuffer) {
}

TEST(CircularBuffer, BufferEmptyAfterCreate) {
	circbuf_t myBuffer;
	uint16_t queue[16];
	uint16_t size=0xffff;

	myBuffer = CircularBuffer_Create(queue);
	size = CircularBuffer_Items(myBuffer);
	TEST_ASSERT_EQUAL_UINT16(0, size);
}

TEST(CircularBuffer, BufferCreateTooManyBuffers) {
	uint16_t i, handle;
	uint16_t queue[16];

	for(i = 0; i < CIRCBUF_MAX_HANDLES; i++) {
		handle = CircularBuffer_Create(queue);
		TEST_ASSERT_EQUAL_INT16(i, handle);
	}
	handle = CircularBuffer_Create(queue);
	TEST_ASSERT_EQUAL_INT16(-1, handle);
}
