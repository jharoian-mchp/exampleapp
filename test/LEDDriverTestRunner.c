#include "unity_fixture.h"

TEST_GROUP_RUNNER(LEDDriver) {
	RUN_TEST_CASE(LEDDriver, LEDsOffAfterCreate);
	RUN_TEST_CASE(LEDDriver, TurnOnLEDOne);
	RUN_TEST_CASE(LEDDriver, TurnOffLEDOne);
}