// LED Driver Tests

// Test list
// - All LEDs are off after driver is initialized
// - A single LED can be turned on
// - A single LED can be turned off
// - Multiple LEDS can be turned on/off
// - Turn on all LEDs
// - Turn off all LEDs
// - Query LED state
// - Check boundary values
// - Check out of bounds values

#include "unity_fixture.h"

#include "LEDDriver.h"

TEST_GROUP(LEDDriver);
	
static uint16_t virtualLEDs;

TEST_SETUP(LEDDriver) {
	LEDDriver_Create(&virtualLEDs);
}

TEST_TEAR_DOWN(LEDDriver) {
	
}

TEST(LEDDriver, LEDsOffAfterCreate) {
	uint16_t virtualLEDs = 0xffff;
	LEDDriver_Create(&virtualLEDs);
	TEST_ASSERT_EQUAL_HEX16(0, virtualLEDs);
}

TEST(LEDDriver, TurnOnLEDOne) {
	LEDDriver_TurnOn(1);

	TEST_ASSERT_EQUAL_HEX16(1, virtualLEDs);
}

TEST(LEDDriver, TurnOffLEDOne) {
	LEDDriver_TurnOn(1);

	LEDDriver_TurnOff(1);

	TEST_ASSERT_EQUAL_HEX16(0, virtualLEDs);
}
