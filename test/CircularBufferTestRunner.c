#include "unity_fixture.h"

TEST_GROUP_RUNNER(CircularBuffer) {
	RUN_TEST_CASE(CircularBuffer, BufferEmptyAfterCreate);
	RUN_TEST_CASE(CircularBuffer, BufferCreateTooManyBuffers);
}